package com.chat.domain.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChatMessage {
    MessageType type;
    String content;
    String sender;

    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }
}
