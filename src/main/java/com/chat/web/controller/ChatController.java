package com.chat.web.controller;

import com.chat.domain.model.ChatMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChatController {

    @SendTo("/topic/public")
    @MessageMapping("/send-message")
    public ChatMessage sendMessage(@Payload ChatMessage message) {
        return message;
    }

    @SendTo("/topic/public")
    @MessageMapping("/join")
    public ChatMessage join(@Payload ChatMessage message, SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", message.getSender());
        return message;
    }
}
